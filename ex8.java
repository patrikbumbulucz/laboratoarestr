import java.util.concurrent.Semaphore;
public class ExecutionThread extends Thread {
    Semaphore b1;
    int sleep_min, sleep_max, activity_min, activity_max;

    public ExecutionThread(Semaphore b1, int sleep_min, int sleep_max, int activity_min, int activity_max) {
        this.b1 = b1;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.activity_min = activity_min;
        this.activity_max = activity_max;
    }

    public void run() {
        while(true) {
            System.out.println(this.getName() + " - STATE 1");
            b1.acquire(1);
            System.out.println(this.getName() + " - STATE 2");
            int k = (int) Math.round(Math.random() * (activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++;
                i--;
            }
            b1.release(1);
            System.out.println(this.getName() + " - STATE 3");
            try {
                Thread.sleep(Math.round(Math.random() * (sleep_max - sleep_min) + sleep_min) * 500);
            } catch (InterruptedException e) {
                e.printStackTrace();                                                              //
            }
            System.out.println(this.getName() + " - STATE 4");
        }

    }
}

public class Main {
    public static void main(String[] args) {
        Semaphore b1=new Semaphore(2);
        new ExecutionThread(monitor, 5, 5, 3, 6).start();
        new ExecutionThread(monitor, 3, 3, 4, 7).start();
        new ExecutionThread(monitor, 6, 6, 5, 7).start();
    }
}
